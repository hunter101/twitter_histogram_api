Twitter Histogram API.

Provides an very brief json endpoint for the twitter histogram example webapp.

Currently only two endpoints defined.

Example

Check server status (should return {"status": "success"} if server is up and running.
http://ec2-52-62-126-116.ap-southeast-2.compute.amazonaws.com:8080/api/v1.0/status

Demo request to pull in latest status update posts from users twitter timeline.
http://ec2-52-62-126-116.ap-southeast-2.compute.amazonaws.com:8080/api/v1.0/twitter/latest_posts/hardtofind_

Additional client IPs will need adding to the CORS policy in index.php to enable cross domain requests.

Requires apache2, composer, PHP > 5.3
Install dependencies using "composer update".

Tests to demonstrate basic functionality can be found in /test folder and run usering vendor/bin/phpunit