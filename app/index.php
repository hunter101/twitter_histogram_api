<?php
require_once 'vendor/autoload.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Replace this with the address of the client application.
// @TODO: refactor into a settings file / environment setting option.
$clientServer = "http://ec2-52-62-126-116.ap-southeast-2.compute.amazonaws.com:80";

$app = new App();

// As the app is a performing the job of a basic rest server, we'll
// set all responses to be in json format.
$app->response->headers->set('Content-Type', 'application/json');

// Add some CORS middleware to simplify the cross site request management.
$corsOptions = array(
    "origin" => array($clientServer),
    "allowCredentials" => True,
    "allowMethods" => array("GET"),
);
$app->add(new \CorsSlim\CorsSlim($corsOptions));
$app->run();