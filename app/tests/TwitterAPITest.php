<?php

use Slim\Environment;
use TwitterOAuth\Serializer\ArraySerializer;
require_once './../vendor/autoload.php';
require (__DIR__ . '/mock_objects/MockAuth.php');

class TwitterAPITest extends PHPUnit_Framework_TestCase
{
    private $app;
    private $serializer;
    private $credentials;

    public function setUp()
    {
        $_SESSION = array();
        $this->serializer = new ArraySerializer();
        $this->credentials = array(
            'consumer_key' => 'v2JhX6iXQkJ0ytFtbjdCVovQs',
            'consumer_secret' => '5ghMrNJZUcK0ILivPsvF3Ae0ASEUua8ioIXgt1mHy8HImvq8Nj',
        );
        $this->app = new App();
    }

    public function testPhpunitWorks() {
        $this->assertEquals(1, 1);
    }

    public function testBasicRoutingWorking()
    {
        Environment::mock(array(
            'PATH_INFO' => '/api/v1.0/status'
        ));
        $response = $this->app->invoke();

        $this->assertContains('success', $response->getBody());
    }

    public function testTwitterTimelineStopsWhenHitsEndofFileEvenThoughWereRequestingMore()
    {
        $count = 500;
        $mockAuth = new \MockAuth($this->credentials, $this->serializer);
        $api = new TwitterAPI($mockAuth, $this->serializer);
        $tweets = $api->getLatestTweets(__DIR__ . "/sample_data/9_tweets.json");
        $this->assertEquals(count($tweets), 9);
    }
}
