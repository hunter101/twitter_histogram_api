<?php

use Slim\Slim as Slim;
use TwitterOAuth\Auth\ApplicationOnlyAuth;
use TwitterOAuth\Serializer\ArraySerializer;


class App extends Slim
{

    const CONSUMER_KEY = "v2JhX6iXQkJ0ytFtbjdCVovQs";
    const CONSUMER_SECRET = "5ghMrNJZUcK0ILivPsvF3Ae0ASEUua8ioIXgt1mHy8HImvq8Nj";

    function __construct(array $userSettings = array())
    {

        parent::__construct($userSettings);

        $this->get('/api/v1.0/status', function () {
            echo json_encode(array('status' => 'success'));
        })->name('status');

        $this->get('/api/v1.0/twitter/latest_posts/:username', function ($username) {

            /**
             * Array with the OAuth tokens provided by Twitter
             *   - consumer_key     Twitter API key
             *   - consumer_secret  Twitter API secret
             */
            $credentials = array(
                'consumer_key' => self::CONSUMER_KEY,
                'consumer_secret' => self::CONSUMER_SECRET,
            );

            /**
             * Instantiate ApplicationOnly api
             *
             * Use the JSONSerializer to make it easy to
             * output data into a standard api format.
             */
            $serializer = new ArraySerializer();
            $auth = new ApplicationOnlyAuth($credentials, $serializer);

            // Instantise the main class to deal with the request.
            $api = new TwitterAPI($auth, $serializer);

            // The php twitter api throws an execption if the username doesn't exist
            // or any other issues with the connection occur.
            // We'll let these bubble up and pass back anything as an error to our api.
            try {
                $tweets = $api->getLatestTweets($username);
            } catch (Exception $e) {
                echo json_encode(array('status' => 'error', 'message' => $e->getMessage()));
                exit();
            }

           echo json_encode(array('status' => 'success', 'data' => json_encode($tweets)));
        });
    }

    /**
     * @return \Slim\Http\Response
     */
    public function invoke()
    {
        $this->middleware[0]->call();
        $this->response()->finalize();
        return $this->response();
    }
}
