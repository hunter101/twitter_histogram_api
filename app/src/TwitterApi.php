<?php

/**
 *  Twitter API Query Builder
 *
 *  An initial skeleton class with a single method to collect x number of
 *  tweets from a users timeline in date order.
 *  Could be extended further at a later point to incorporate
 *  additional / more complex queries to work around the standard api limits.
 *
 *  @author Andy Hunter
 *  @see https://github.com/ricardoper/TwitterOAuth
 */
class TwitterAPI
{
    protected $auth;
    protected $serializer;

    public function __construct(TwitterOAuth\Auth\AuthAbstract $auth, TwitterOAuth\Serializer\SerializerInterface $serializer)
    {
        $this->setAuth($auth);
        $this->setSerializer($serializer);
    }

    /**
     * Grab a bunch of latest tweets form the twitter API.
     *
     * Because the twitter api. is limited to returning only 200 tweets per request
     * (and removes any retweets, replies etc. after collecting the 200 - generating even
     * less in practise.  We'll loop over the twitter request to collect the required
     * number of tweets.
     *
     * @param $username
     * @param int $quantityRequired
     * @return array
     */
    public function getLatestTweets($username, $quantityRequired = 500)
    {

        /**
         * Returns a collection of the most recent Tweets posted by the user
         * https://dev.twitter.com/docs/api/1.1/get/statuses/user_timeline
         */
        $params = array(
            'screen_name' => $username,
            'count' => $quantityRequired,
            'exclude_replies' => true
        );

        /**
         * The Twitter API only allows us to get 200 responses per request,
         * we'll need to call the API x amount of times and append the results.
         */
        $moreTweets = TRUE;      // Flag to check if the next request is able able to return any more tweets.
        $lastTweetId = NULL;     // Used to grab the next collection of tweets starting from this number.
        $tweets = [];

        while ((count($tweets) < $quantityRequired) && $moreTweets) {

            if ($lastTweetId) {
                $params['max_id'] = $lastTweetId;
            }
            /**
             * Send a GET call with set parameters.
             */
            $twitterResponse = $this->auth->get('statuses/user_timeline', $params);

            // We need to get rid of the first tweet returned as this is the same as
            // the last tweet in the previous batch, only if we're not on the first iteration.
            if ($lastTweetId) {
                array_shift($twitterResponse);
            }

            // Check if we've reached the end of the users tweet history.
            $numberOfTweetsReturned = count($twitterResponse);
            if ($numberOfTweetsReturned === 0) {
                $moreTweets = FALSE;
                continue;
            }

            $lastTweetId = end($twitterResponse)['id'];
            $tweets += array_merge($tweets, $twitterResponse);
        }

        return array_slice($tweets, 0, $quantityRequired);
    }

    protected function setAuth(TwitterOAuth\Auth\AuthAbstract $auth)
    {
        $this->auth = $auth;
        return $auth;
    }

    protected function setSerializer(TwitterOAuth\Serializer\SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        return $serializer;
    }
}
